import "./App.css"
import Home from "./pages/home";
import EthicalHacking from "./pages/ethical-hacking"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./components/login"
import Python from "./blogs/python"
import Blogtemplate from "./blogs/blog-template"
import Texteditor from "./pages/texteditor"
import Bloghomepage from "./pages/bloghomepage"
import AprilContent from "./blogs/2023/April/april-content"
//npm install react-scripts@latest
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route>
          <Route path="/" element={<Bloghomepage />} />
          <Route path="/home" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/python" element={<Python />}></Route>
          <Route path="/ethical-hacking" element={<EthicalHacking />} />
          <Route path="/blogtemplate" element={<Blogtemplate />}></Route>
          <Route path="/2023/April/AprilContent" element={<AprilContent />}></Route>
          <Route path="/texteditor" element={<Texteditor />}></Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
  
}
export default App;