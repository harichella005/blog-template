import "../App.css"
import ethical from "../assets/ethical-hacker.png";
import { SocialIcon } from 'react-social-icons';
import propic from "../assets/user.png";


const App = () => {
    return (
      <div className="App">
        <header className="App-header">
        <>
        <div className="content">
        <div className="user-profile">
          <img src={propic} class="profile-pic" alt="..."></img>
          <h2 style={{ color: "white", fontFamily: 'initial'}}>Hariharan C</h2>
          <div className='post-details'>
            <h5><span>Blog: </span>Ethical Hacking</h5>
            <h5><span>Uploaded: </span>12-March-2023</h5>
          </div>
          <div className='socialicon'>
            <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='facebook' bgColor='blue' fgColor='white'></SocialIcon>
            <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='instagram' bgColor='#d62976' fgColor='white'></SocialIcon>
            <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='linkedin' bgColor='#00A0DC' fgColor='white'></SocialIcon>
            <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='youtube' bgColor='red' fgColor='white'></SocialIcon>
          </div>
        </div>
          <img className="ethicalimg" src={ethical} alt=""></img>
          <p>Before learning ethical hacking, first know what is Hacking.</p>
          <h3>Hacking</h3>
          <p>Hacking is defined as gaining access to a system to which you are not authorized. Logging into an 
            email account to which you are not allowed to have access, getting access to a remotely located computer to 
            which you are not supposed to have access, and reading material to which you are not authorized to 
            have access are all examples of hacking. A system can be hacked in a variety of ways. The first 
            known hacking incident occurred in 1960 at MIT, and the term "hacker" was coined at the same time.</p>
          <h3>What is ethical hacking?</h3>
          <p>Ethical hacking is also known as <strong>White hat Hacking or Penetration Testing.</strong> Ethical hacking 
            involves an authorized attempt to gain unauthorized access to a computer system or data. 
            Ethical hacking is used to improve the security of the systems and networks by fixing the 
            vulnerability found while testing. 
          </p>
          <p>Ethical hackers improve the security posture of an organization. Ethical hackers use the 
            same tools, tricks, and techniques that malicious hackers used, but with the permission of 
            the authorized person. The purpose of ethical hacking is to improve the security and to 
            defend the systems from attacks by malicious users.
          </p>
          <h2>Types of Hacking</h2>
          <p>We can define hacking into different categories, based on what is being hacked. These are as follows:</p>
          <ol className="ol">
            <li>Network Hacking</li>
            <li>Website Hacking</li>
            <li>Computer Hacking</li>
            <li>Password Hacking</li>
            <li>Email Hacking</li>
          </ol>
          <h3>Network Hacking</h3>
          <p>Network hacking means gathering information about a network with the intent to harm the network system and hamper its operations using the various tools like Telnet, NS lookup, Ping, Tracert, etc. </p>
          <h3>Website Hacking</h3>
          <p>Website hacking means taking unauthorized access over a web server, database and make a change in the information. </p>
          <h3>Computer Hacing</h3>
          <p>Computer hacking means unauthorized access to the Computer and steals the information from PC like Computer ID and password by applying hacking methods. </p>
          <h3>Password Hacking</h3>
          <p>Password hacking is the process of recovering secret passwords from data that has been already stored in the computer system.</p>
          <h3>Email Hacking</h3>
          <p>Email hacking means unauthorized access on an Email account and using it without the owner's permission.</p>
          <h3>Advantages of Hacking</h3>
          <p>There are various advantages of hacking:</p>
          <ol className="ol">
            <li>It is used to recover the lost of information, especially when you lost your password.</li>
            <li>It is used to perform penetration testing to increase the security of the computer and network.</li>
            <li>It is used to test how good security is on your network.</li>
          </ol>
          <h3>Disadvantages of Hacking</h3>
          <p>There are various disadvantages of hacking: </p>
          <ol className="ol">
            <li>It can harm the privacy of someone.</li>
            <li>Hacking is illegal.</li>
            <li>Criminal can use hacking to their advantage.</li>
            <li>Hampering system operations.</li>
          </ol>
          <h3>Types of Hackers</h3>
          <p>Hackers can be classified into three different categories:</p>
          <ol className="ol">
            <li>Black Hat Hacker</li>
            <li>White Hat Hacker</li>
            <li>Grey Hat Hacker</li> 
          </ol>
          <h3>Black Hat Hacker</h3>
          <p>Black-hat Hackers are also known as an Unethical Hacker or a Security Cracker. These people hack 
            the system illegally to steal money or to achieve their own illegal goals. They find banks or 
            other companies with weak security and steal money or credit card information. They can also 
            modify or destroy the data as well. Black hat hacking is illegal.</p>
          <h3>White Hat Hacker</h3>
          <p>White hat Hackers are also known as Ethical Hackers or a Penetration Tester. White hat hackers 
            are the good guys of the hacker world. These people use the same technique used by the black hat 
            hackers. They also hack the system, but they can only hack the system that they have permission 
            to hack in order to test the security of the system. They focus on security and protecting 
            IT system. White hat hacking is legal.</p>
          <h3>Grey Hat Hacker</h3>
          <p>Gray hat Hackers are Hybrid between Black hat Hackers and White hat hackers. They can hack any 
            system even if they don't have permission to test the security of the system but they will never 
            steal money or damage the system. In most cases, they tell the administrator of that system. 
            But they are also illegal because they test the security of the system that they do not have 
            permission to test. Grey hat hacking is sometimes acted legally and sometimes not.</p>
        </div>
        <section className="footer">
          <h1>Footer</h1>
        </section>
        </>
        </header>
      </div>
        
    )
};
export default App;