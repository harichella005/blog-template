//Required libraries for the blog page
import "../App.css"
import { SocialIcon } from 'react-social-icons';
import propic from "../assets/user.png";
import Python from "../assets/python-original-wordmark.svg"

//App starts here
const App = () => {
    return (
      <div className="App">
        <header className="App-header">
        <>
        {/* Blogger profile card section, this section contains the blogger picture, name, blog title, 
        on which date blog uploaded, and social media icons of the blogger */}
        <section className="user-profile">
            <img src={propic} class="profile-pic" alt="..."></img>
            <h2 style={{ color: "white", fontFamily: 'initial'}}>Blogger Name</h2>
            <div className='post-details'>
                <h5><span>Blog: </span>Title</h5>
                <h5><span>Uploaded: </span>15/03/2023</h5>
            </div>
            <div className='socialicon'>
                <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='facebook' bgColor='blue' fgColor='white'></SocialIcon>
                <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='instagram' bgColor='#d62976' fgColor='white'></SocialIcon>
                <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='linkedin' bgColor='#00A0DC' fgColor='white'></SocialIcon>
                <SocialIcon style={{ height: 25, width: 25 }} className="soicon" url='http://google.com' network='youtube' bgColor='red' fgColor='white'></SocialIcon>
            </div>
        </section>
        {/* This is section holds the blog content  */}
        <section className="content">
            {/* Blog image that will appears on the center of the blog starts   */}
            <div>
                <img className="blogtitleimg" src={Python} alt=""></img>
            </div>
            <br></br>
            <h2>Heading for the content</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the
            1500s, when an unknown printer took a galley of type and scrambled it to
            make a type specimen book. It has survived not only five centuries, but
            also the leap into electronic typesetting, remaining essentially
            unchanged. It was popularised in the 1960s with the release of Letraset
            sheets containing Lorem Ipsum passages, and more recently with desktop
            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            {/* image if needed for the content */}
            <img src={Python} style={{ height: 325, width: 175 }} alt="python"></img>
            {/* Paragraph  */}
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the
            1500s, when an unknown printer took a galley of type and scrambled it to
            make a type specimen book. It has survived not only five centuries, but
            also the leap into electronic typesetting, remaining essentially
            unchanged. It was popularised in the 1960s with the release of Letraset
            sheets containing Lorem Ipsum passages, and more recently with desktop
            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            {/* Content should be listed, example followings are like that */}
          <h2>Types of Hacking</h2>
          <p>We can define hacking into different categories, based on what is being hacked. These are as follows:</p>
          <ol className="ol">
            <li>Network Hacking</li>
            <li>Website Hacking</li>
            <li>Computer Hacking</li>
            <li>Password Hacking</li>
            <li>Email Hacking</li>
          </ol>
        </section>
        {/* Footer section for the current page */}
        <section className="footer">
          <h1>Footer</h1>
        </section>
        </>
        </header>
      </div>
        
    )
};
export default App;