import TimeAgo from 'react-timeago'
import enStrings from 'react-timeago/lib/language-strings/en'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'

function App() {
    const formatter = buildFormatter(enStrings)
    return(
        <TimeAgo date='Apr 1, 2023 19:51:00' formatter={formatter} />
    )

  }

export default App;