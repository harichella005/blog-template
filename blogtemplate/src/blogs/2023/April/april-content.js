import React from 'react'
import img1 from "../../../assets/wd.png"
import "../../../App.css"
import user from "../../../assets/user-solid.svg"
import Onenote from "../../../blogs/2023/April/asserts/onenote.png"
import Pam from "../../../blogs/2023/April/asserts/pam.png"
import Timetest from "../../../pages/timetest"
function homeBlog() {
    return (
        //Post content only for 5 post should appear
        <div className="homebody">
            <header className='homebody-header'>
                <div className='home-header'>
                    <h1>LOGO</h1>
                    <a href='/vulnerabilities'>Home</a>
                    <a href='/vulnerabilities'>Vulnerabilities</a>
                </div>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                <h2>April-4-2023</h2>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={Onenote} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Microsoft Tightens OneNote Security by Auto-Blocking 120 Risky File Extensions</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                <h2>April-4-2023</h2>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={Pam} alt='...'></img>
                        <div className='blog-title'>
                            <h1>"It's The Service Accounts, Stupid": Why Do PAM Deployments Take (almost) Forever To Complete?</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                <h2>April-4-2023</h2>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                <h2>April-4-2023</h2>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <section className='home-sec'>
                    <a href='/blogtemplate'>
                        <img src={img1} alt='...'></img>
                        <div className='blog-title'>
                            <h1>Western Digital Hit by Network Security Breach - Critical Services Disrupted!</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book. It has survived not only five centuries, but
                            also the leap into electronic typesetting, remaining essentially
                            unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop
                            publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className="blog-author">
                            <span>
                                <img src={user} alt="..."></img>
                            </span>
                            <span>
                                <h2>By: Hariharan</h2>
                            </span>
                            <span className='post-date'>
                                {/* <h2>April-4-2023</h2> */}
                                <Timetest></Timetest>
                            </span>
                        </div>
                    </a>
                </section>
                <div className='next-button'>
                    <a href='/2023/04/03/ethicalhacing'>Next Page</a>
                </div>
                <div className='next-button'>
                    <a href='/'>Home</a>
                </div>
                <section className='footer'>
                    <h1>Footer</h1>
                </section>
            </header>
        </div>
    )
}

export default homeBlog
