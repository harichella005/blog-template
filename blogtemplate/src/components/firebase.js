// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBOEw9Fw1uSFnoR75TDtk2VKjyV8J-8hXI",
  authDomain: "blogtemplate-1fbbf.firebaseapp.com",
  projectId: "blogtemplate-1fbbf",
  storageBucket: "blogtemplate-1fbbf.appspot.com",
  messagingSenderId: "424244158481",
  appId: "1:424244158481:web:50958edee564ae6411d1b0",
  measurementId: "G-HZHR92TS4S"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);