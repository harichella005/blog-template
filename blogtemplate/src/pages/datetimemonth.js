import React from "react";

export default function App() {
  const current = new Date();
  const date = `${current.getDate()}/${current.getMonth()+1}/${current.getFullYear()}`;
  return (
    <div className="montdateyear">
      <p>{date}</p>    
    </div>
  );
}