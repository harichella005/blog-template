import '../App.css';
import "../dist/css/bootstrap.min.css";
import Python from "../assets/python-original-wordmark.svg"
import AWS from "../assets/amazonwebservices-original-wordmark.svg"
import conimg from "../assets/ethical-hacking-tutorial.png";
import Digital from "../assets/digital-marketing.png"
import Django from "../assets/django-plain-wordmark.svg"
import Bash from "../assets/bash-original.svg"
import Docker from "../assets/docker-original-wordmark.svg"
import Git from "../assets/git-original-wordmark.svg"
const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <section>
          <button className="create-blog" onClick={()=> window.open("/texteditor", "_self")}>Create Blog</button>
        </section>
        <div class="row">
          <div class="col-md">
            <div class="immg">
              <a href="/ethical-hacking">
                <img className="blogimg" src={conimg} alt=""></img>
                <div>
                  <h3 className="blogname" fgColor='white'>Ethical Hacking</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-md">
            <a href="/python">
              <img className="blogimg" src={Python} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Python</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={AWS} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>AWS</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={Digital} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Digital Marketing</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={Django} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Django</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={Bash} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Bash</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={Docker} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Docker</h3>
              </div>
            </a>
          </div>
          <div class="col-md">
            <a href="/ethical-hacking">
              <img className="blogimg" src={Git} alt=""></img>
              <div>
                <h3 className="blogname" fgColor='white'>Git</h3>
              </div>
            </a>
          </div>
        </div>
        <section>
          <div>
            <button className="moreblogs" onClick={()=> window.open("#", "_blank")}>More Blogs</button>
          </div>
        </section>
        <section className="footer">
          <h1>Footer</h1>
        </section>
      </header>
    </div>
  )
};

export default App;